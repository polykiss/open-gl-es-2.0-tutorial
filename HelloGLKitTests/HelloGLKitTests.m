//
//  HelloGLKitTests.m
//  HelloGLKitTests
//
//  Created by Ezra Paulekas on 7/8/14.
//  Copyright (c) 2014 Ezra Paulekas. All rights reserved.
//

#import <XCTest/XCTest.h>

@interface HelloGLKitTests : XCTestCase

@end

@implementation HelloGLKitTests

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample
{
    XCTFail(@"No implementation for \"%s\"", __PRETTY_FUNCTION__);
}

@end
