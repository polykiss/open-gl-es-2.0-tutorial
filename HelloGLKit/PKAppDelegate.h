//
//  PKAppDelegate.h
//  HelloGLKit
//
//  Created by Ezra Paulekas on 7/8/14.
//  Copyright (c) 2014 Ezra Paulekas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GLKit/GLKit.h>
#import <QuartzCore/QuartzCore.h>

@interface PKAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
