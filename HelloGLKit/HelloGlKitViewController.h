//
//  HelloGlKitViewController.h
//  HelloGLKit
//
//  Created by Ezra Paulekas on 7/8/14.
//  Copyright (c) 2014 Ezra Paulekas. All rights reserved.
//

#import <GLKit/GLKit.h>

@interface HelloGlKitViewController : GLKViewController {
    float _curRed;
    BOOL _increasing;
}
@property (retain, nonatomic) EAGLContext *context;
@property (retain, nonatomic) GLKBaseEffect *effect;


@end

/*
@implementation HelloGlKitViewController
@synthesize context = _context;
@end
*/