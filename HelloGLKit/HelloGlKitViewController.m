//
//  HelloGlKitViewController.m
//  HelloGLKit
//
//  Created by Ezra Paulekas on 7/8/14.
//  Copyright (c) 2014 Ezra Paulekas. All rights reserved.
//

#import "HelloGlKitViewController.h"

/*
@interface HelloGlKitViewController ()

@end
*/


// study this code in the future

typedef struct  {
    float Position[3];
    float Color[4];
} Vertex;

const Vertex Vertices[] = {
    {{1, -1, 0}, {1, 0, 0, 1}},
    {{1, 1, 0}, {0,1,0,1}},
    {{-1, 1, 0}, {0, 0, 1, 1}},
    {{-1, -1, 0}, {0, 0, 0, 1}}
};

const GLubyte Indices[] = {
    0, 1, 2,
    2, 3, 0
};


@implementation HelloGlKitViewController    {
    GLuint _vertexBuffer;
    GLuint _indexBuffer;
    float _rotation;
}

@synthesize context = _context;
@synthesize effect = _effect;



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}




-(void)setupGL  {
    // sets the context just in case something else has changed it
    [EAGLContext setCurrentContext:self.context];
    
    self.effect = [[GLKBaseEffect alloc] init];
    
    // creates a new buffer object
    glGenBuffers(1, &_vertexBuffer);
    // tells GK vertexBuffer is actually a vertex buffer array
    glBindBuffer(GL_ARRAY_BUFFER, _vertexBuffer);
    // sends the data over to Open GL
    glBufferData(GL_ARRAY_BUFFER, sizeof(Vertices), Vertices, GL_STATIC_DRAW);
    
    glGenBuffers(1, &_indexBuffer);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _indexBuffer);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(Indices), Indices, GL_STATIC_DRAW);
}



- (void)viewDidLoad
{
    [super viewDidLoad];

    self.context = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES2];
    
    if(!self.context)   {
        NSLog(@"Failed to create ES context");
    }
    
    GLKView *view = (GLKView *)self.view;
    view.context = self.context;
    
    [self setupGL];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


#pragma mark - GLKViewDelegate

-(void)glkView:(GLKView *)view drawInRect:(CGRect)rect  {
    glClearColor(_curRed, 0.0, 0.0, 1.0);
    glClear(GL_COLOR_BUFFER_BIT);
    
    // needs to be called any time we adjust the translation matrix
    [self.effect prepareToDraw];
    
    // tell opengl which vertex buffer objects to use
    // bind vertex buffers created earlier
    glBindBuffer(GL_ARRAY_BUFFER, _vertexBuffer);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _indexBuffer);
    
    // next enable the pre-defined vertex attributes we want GLKBaseEffect to use
    // enable attribute for position
    glEnableVertexAttribArray(GLKVertexAttribPosition);
    // feed correct values to input variables for the vertex shader
    // first parameter is the type of attribute (position)
    // second parameter is the number of of values for each vertex (3)
    // type of each value
    // fourth parameter is always false (why?)
    // fifth is size of the "stride" or data struct per vertex
    // offset within the structure to find the data...use the offsetof function
    glVertexAttribPointer(GLKVertexAttribPosition, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (const GLvoid *)offsetof(Vertex, Position));
    // enable attribute for color
    glEnableVertexAttribArray(GLKVertexAttribColor);
    // feed correct values for color
    glVertexAttribPointer(GLKVertexAttribColor, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), (const GLvoid *)offsetof(Vertex, Color));
    
    // draw the geometry
    // first parameter is the manner of drawing the vertices there are different options:
    //      GL_LINE_STRIP
    //      GL_TRIANGLE_FAN
    //      GL_TRIANGLES
    // second parameter is the count of vertices to render...
    //      this is a "C trick" that divides the size of the array by the size of the first element
    // third parameter is the data type of each individual index in the indices array
    // fourth should be a pointer to the Indices since were using VBOs its a special case (???)
    glDrawElements(GL_TRIANGLES, sizeof(Indices)/sizeof(Indices[0]), GL_UNSIGNED_BYTE, 0);
}

#pragma mark - GLKViewControllerDelegate

- (void)update {
    if (_increasing) {
        _curRed += 1.0 * self.timeSinceLastUpdate;
    } else {
        _curRed -= 1.0 * self.timeSinceLastUpdate;
    }
    if (_curRed >= 1.0) {
        _curRed = 1.0;
        _increasing = NO;
    }
    if (_curRed <= 0.0) {
        _curRed = 0.0;
        _increasing = YES;
    }
    
    // calculate the aspect ratio of the current view
    float aspect = fabsf(self.view.bounds.size.width / self.view.bounds.size.height);
    // use built in helper function to create a perspective matrix
    GLKMatrix4 projectionMatrix = GLKMatrix4MakePerspective(GLKMathDegreesToRadians(65.0f), aspect, 4.0f, 10.0f);
    // set the projection matrix
    self.effect.transform.projectionMatrix = projectionMatrix;
    
    // translation matrix to move the geometry back 6 untis (from 0)
    GLKMatrix4 modelViewMatrix = GLKMatrix4MakeTranslation(0.0f, 0.0f, -6.0f);
    // make the cube rotate a bit
    _rotation += 90 * self.timeSinceLastUpdate;
    // new matrix to apply _rotation vertices
    modelViewMatrix = GLKMatrix4Rotate(modelViewMatrix, GLKMathDegreesToRadians(_rotation), 0, 0, 1);
    // set the model view matrix to the new model view matrix
    self.effect.transform.modelviewMatrix = modelViewMatrix;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event    {
    self.paused = !self.paused;
    
    NSLog(@"timeSinceLastUpdate: %f", self.timeSinceLastUpdate);
    NSLog(@"timeSinceLastDraw: %f", self.timeSinceLastDraw);
    NSLog(@"timeSinceFirstResume: %f", self.timeSinceFirstResume);
    NSLog(@"timeSinceLastResume: %f", self.timeSinceLastResume);
}

@end
