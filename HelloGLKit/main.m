//
//  main.m
//  HelloGLKit
//
//  Created by Ezra Paulekas on 7/8/14.
//  Copyright (c) 2014 Ezra Paulekas. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "PKAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([PKAppDelegate class]));
    }
}
